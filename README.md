![](logo.png "project logo")

## Project is still in progress. There is missing, empty or incomplete pages in wiki.

You can follow project development in GitHub Projects.

#### Still, feel free to check out Component Lists page with links to AliExpress - you can find plenty of cool stuff there.

### What is this?

This repository contains wiki, manual and source codes for AR2Glass project, Arduino-based AR glasses.

### Why?

I am doing this because:
1. I want to
2. To learn several important skills, refresh my knowledge in electricity, etc.
3. Because it's going to be awesome.

## This project is ~rip-off from~ inspired by [this](https://hackaday.io/project/12211-arduino-glasses-a-hmd-for-multimeter) project!
It is! Alan's project is great. I really envy his 3d modelling skills. Anyway, I really would like to take this project and push it slightly forward.
