// Screen
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// Bluetooth module
#include <SoftwareSerial.h>

// Display setup
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 32)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

// Bluetooth setup
SoftwareSerial mySerial(10, 11); // RX, TX

void setup() {
	// Init Bluetooth and regular serial
	Serial.begin(57600);
	mySerial.begin(57600);

	// Init Display
	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

	// Clear the buffer.
	display.clearDisplay();

	/*
	// fonttest lists all of available glyphs one after another
	fonttest();
	display.display();
	delay(2000);
	display.clearDisplay();
	*/

	// text display tests
	// setup font
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setCursor(0,0);
	display.println(utf8rus("AR2Glass"));
	display.print("0x"); display.println(0xDEADBEEF, HEX);
	display.display();
	delay(2000);
	display.clearDisplay();
}


void loop() {
	if (mySerial.available()) {
		Serial.write(mySerial.read());
	}

	if (Serial.available()) {
		mySerial.write(Serial.read());
	}

}

void fonttest(void) {
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setCursor(0,0);

	for (uint8_t i = 0; i < 255; i++) {
		if (i == '\n') continue;
		display.write(i);
		if ((i > 0) && (i % 30 == 0)) {
			display.display();
			delay(3000);
			display.clearDisplay();
			display.setCursor(0,0);
		}
	}
	display.display();
}

// Recode russian fonts from UTF-8 to Windows-1251
// Без данной функции мы не сможем нормально выводить русские символы на экран
String utf8rus(String source) {
	int i, k;
	String target;
	unsigned char n;
	char m[2] = { '0', '\0' };

	k = source.length(); i = 0;

	while (i < k) {
		n = source[i]; i++;

		if (n >= 0xC0) {
			switch (n) {
				case 0xD0: {
					n = source[i]; i++;
					if (n == 0x81) { n = 0xA8; break; }
					if (n >= 0x90 && n <= 0xBF) n = n + 0x2F;
					break;
				}
				case 0xD1: {
					n = source[i]; i++;
					if (n == 0x91) { n = 0xB8; break; }
					if (n >= 0x80 && n <= 0x8F) n = n + 0x6F;
					break;
				}
			}
		}

		m[0] = n; target = target + String(m);
	}

	return target;
}

